# UE4/UDK Remote Controller Library #

Note: there's a newer example of how to get this working with UE4 native code and Blueprint: https://bitbucket.org/omigamedev/ue4_remotesteeringwheel

This is a Windows library that uses WinSock to connect via TCP to and Android smartphone and use the sensor fusion to get the orientation. The data is then sent to the client that can map it directly to the player orientation, or make whatever needed in the game.

Here's a video that show how it works: https://www.youtube.com/watch?v=bSVIWE45aJ0

![yt-video.jpg](https://bitbucket.org/repo/MLaj8a/images/815255902-yt-video.jpg)