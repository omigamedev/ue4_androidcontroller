package it.omidev.ue4droidpad;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.nio.charset.Charset;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Locale;

import android.app.Activity;
import android.graphics.Point;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MotionEvent;
import android.widget.TextView;

public class MainActivity extends Activity implements SensorEventListener
{
	public static TextView txtBox;
	public MessageQueue mqout;

	public SensorManager smgr;
	public Sensor sacc;
	public Sensor smag;
	public Sensor sgyr;
	public int gyroSamples;
	
	WakeLock wakeLock;
	PowerManager powerManager;
	
	boolean pushDown;
	boolean pushSent;

	CircularBuffer sprint;
	int last_sprint;

	int last_theta;
	long timerStart;
	int width;
	int height;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		powerManager = (PowerManager) getSystemService(POWER_SERVICE);
		wakeLock = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK, "UE4_DroidPad_WakelockTag");

		txtBox = (TextView) findViewById(R.id.txtBox);
		txtBox.setText("start\n");

		mqout = new MessageQueue();
		sprint = new CircularBuffer(30);
		pushDown = false;
		pushSent = false;

		listInterfaces();

		smgr = (SensorManager) getSystemService(SENSOR_SERVICE);
		sacc = smgr.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		smag = smgr.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
		sgyr = smgr.getDefaultSensor(Sensor.TYPE_GYROSCOPE);

		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		width = size.x;
		height = size.y;

		Thread sockListener = new Thread()
		{
			@Override
			public void run()
			{
				try
				{
					ServerSocket serv = new ServerSocket(54321);
					txtBox.append("listening " + serv.getInetAddress() + ":" + serv.getLocalPort() + "\n");
					while(true)
					{
						Socket sock = serv.accept();
						log("connected to " + sock.getRemoteSocketAddress());
						DataInputStream in = new DataInputStream(sock.getInputStream());
						DataOutputStream out = new DataOutputStream(sock.getOutputStream());
						boolean connected = true;
						while(connected)
						{
							try
							{
								byte[] data = new byte[1024];
								int l = in.read(data);

								// if the length of the data read is -1 means
								// that
								// the buffer
								// reader is not valid anymore.
								if(l == -1)
								{
									connected = false;
									log("disconnected");
									sock.close();
									break;
								}
								log("message (" + l + "): " + new String(data, 0, l - 1));
								while(true)
								{
									String s = mqout.getMessage();
									out.write(s.getBytes(Charset.defaultCharset()));
									out.flush();
								}

							}
							catch(IOException e)
							{
								e.printStackTrace();
							}
						}
					}
				}
				catch(IOException e)
				{
					e.printStackTrace();
					log("socket error");
				}
				super.run();
			}
		};
		sockListener.start();
	}

	private void listInterfaces()
	{
		Enumeration<NetworkInterface> list;
		try
		{
			list = NetworkInterface.getNetworkInterfaces();

			while(list.hasMoreElements())
			{
				NetworkInterface iface = (NetworkInterface) list.nextElement();

				if(iface == null)
					continue;

				if(!iface.isLoopback() && iface.isUp())
				{
					// System.out.println("Found non-loopback, up interface:" +
					// iface);
					Iterator<InterfaceAddress> it = iface.getInterfaceAddresses().iterator();
					while(it.hasNext())
					{
						InterfaceAddress address = (InterfaceAddress) it.next();
						// System.out.println("Found address: " + address);
						if(address == null)
							continue;
						// InetAddress broadcast = address.getBroadcast();
						if(address.getAddress().getHostAddress().indexOf("192") > -1)
						{
							txtBox.append("Found iface: " + address.getAddress().getHostAddress() + "\n");
						}
					}
				}
			}
		}
		catch(SocketException ex)
		{
			log("Error while getting network interfaces");
			ex.printStackTrace();
		}
	}

	@Override
	protected void onResume()
	{
		timerStart = 0;
		last_sprint = -1;
		last_theta = -1;

		smgr.registerListener(this, sacc, SensorManager.SENSOR_DELAY_FASTEST);
		smgr.registerListener(this, smag, SensorManager.SENSOR_DELAY_FASTEST);
		smgr.registerListener(this, sgyr, SensorManager.SENSOR_DELAY_FASTEST);
		
		wakeLock.acquire();
		
		SensorFusion.Init();
		
		super.onResume();
	}

	@Override
	protected void onPause()
	{
		smgr.unregisterListener(this);
		wakeLock.release();
		super.onPause();
	}
	
	@Override
	protected void onDestroy() 
	{
		super.onDestroy();
	}

	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		switch(event.getAction())
		{
		case MotionEvent.ACTION_DOWN:
			pushDown = true;
			pushSent = false;
			break;
		case MotionEvent.ACTION_MOVE:
			break;
		case MotionEvent.ACTION_UP:
			pushDown = false;
			break;
		default:
			break;
		}
		return super.onTouchEvent(event);
	}

	public static void log(String s)
	{
		Log.d("ue4pad", s);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy)
	{

	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		//super.onBackPressed();
	}

	@Override
	public void onSensorChanged(SensorEvent event)
	{
		float x = event.values[0];
		// Swap y and z
		float y = event.values[2];
		float z = event.values[1];
		
		switch(event.sensor.getType())
		{
		case Sensor.TYPE_ACCELEROMETER:
			SensorFusion.UpdateAccelerometer(x, y, z);
			sprint.add((float)Math.floor(event.values[0] * 100));
			break;
		case Sensor.TYPE_MAGNETIC_FIELD:
			SensorFusion.UpdateMagnetometer(x, y, z);
			break;
		case Sensor.TYPE_GYROSCOPE:
			SensorFusion.UpdateGyroscope(x, y, z);
			gyroSamples++;
			break;
		}

		long timerStop = System.nanoTime();
		double dtSend = (timerStop - timerStart) * 1E-9;

		if(dtSend > 0.05)
		{
			log("angle " + SensorFusion.GetCurrentAngle());
			int theta = (int)((SensorFusion.GetCurrentAngle() + 180.0) * 100);
			int sprint_avg = (int) sprint.average().x;
			int push = pushDown && !pushSent ? 1 : 0;
			String msg = String.format(Locale.getDefault(), "%d|%d|%d*", theta, sprint_avg, push);
			if(last_sprint != sprint_avg || last_theta != theta)
			{
				last_sprint = sprint_avg;
				last_theta = theta;
				log(msg);
				mqout.postMessage(msg);
			}
			timerStart = timerStop;
		}

	}
}
