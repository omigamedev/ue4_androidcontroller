package it.omidev.ue4droidpad;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.nio.charset.Charset;

/**
 * Peer is a thread that handle a TCP connection between a local and a remote
 * socket. When data arrives from the remote socket it is appended to a specific
 * queue that consumes the messages.
 */
public class Peer extends Thread
{
	private Socket sock;
	private DataOutputStream out;
	private DataInputStream in;
	private MessageQueue mq;
	private boolean connected;

	public Peer(Socket s, MessageQueue mq) throws Exception
	{
		this.sock = s;
		this.out = new DataOutputStream(sock.getOutputStream());
		this.in = new DataInputStream(sock.getInputStream());
		this.mq = mq;
		this.connected = true;
	}

	public boolean isConnected()
	{
		return connected;
	}

	public String getIp()
	{
		return sock.getInetAddress().getHostAddress();
	}

	public void sendMessage(String m)
	{
		try
		{
			out.write(m.getBytes(Charset.defaultCharset()));
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public String toString()
	{
		return "Peer [sock=" + sock + "]";
	}

	@Override
	public void run()
	{
		while(isConnected())
		{
			try
			{
				byte[] data = new byte[1024];
				int l = in.read(data);

				// if the length of the data read is -1 means that the buffer
				// reader is not valid anymore.
				if(l == -1)
				{
					connected = false;
					break;
				}
				String m = new String(data, 0, l - 1);
				mq.postMessage(m);
			}
			catch(IOException e)
			{
				// e.printStackTrace();
				try
				{
					sock.close();
				}
				catch(IOException e1)
				{
					e1.printStackTrace();
				}
				connected = false;
			}
		}

		// Post the last message to notify the consumer that the connection has
		// been lost.
		mq.postMessage("end");

		System.out.println(this + " terminated");
	}
}
