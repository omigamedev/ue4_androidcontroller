package it.omidev.ue4droidpad;

public class CircularBuffer 
{
	private int count;
	private int max;
	private int index;
	private Vector3[] values;

	public CircularBuffer(int capacity) 
	{
		values = new Vector3[capacity];
		count = 0;
		index = 0;
		max = capacity;
	}

	public void add(Vector3 val) 
	{
		values[index] = val;
		index = (index + 1) % max;
		count = Math.min(max, count + 1);
	}
	
	public void add(float[] val)
	{
		add(new Vector3(val[0], val[1], val[2]));
	}
	
	public void add(float single)
	{
		add(new Vector3(single, 0, 0));
	}

	public Vector3 average() 
	{
		float x = 0;
		float y = 0;
		float z = 0;
		for (int i = 0; i < count; i++) 
		{
			x += values[i].x;
			y += values[i].y;
			z += values[i].z;
		}
		return new Vector3(x / count, y / count, z / count);
	}

	public class Vector3
	{
		float x, y, z;
		
		public Vector3() 
		{
			x = 0;
			y = 0;
			z = 0;
		}
		
		public Vector3(float x, float y, float z) 
		{
			this.x = x;
			this.y = y;
			this.z = z;
		}
	}
}
